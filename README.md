# Overview
KennyLoggings is a tamper evident logging system that generates integrity proofs for log events at the moment they are generated in the kernel (i.e., synchronously). 
It is designed to defend against race condition attacks on audit frameworks.
Both these attacks and the design of KennyLoggings are described in the paper:

- [_Logging to the Danger Zone: Race Condition Attacks and Defenses on System Audit Frameworks_][paper] (__CCS 2020__)

This repository contains the source code of the prototypes of KennyLoggings that we used for the defense evaluation of the paper.

# Prototypes
We include the following prototypes:

1. Standalone kernel module to test the basic functionality outside of a specific logging framework. The provided kernel module implements the optimized version of KennyLoggings that precomputes the signing keys.
2. Two kernel patches to add KennyLoggings to the Linux Audit subsystem.
    - The first kernel patch (`optimized.patch`) is for the optimized version of KennyLoggings that precomputes signing keys (like the kernel module). We recommend this version for performance.
    - The second kernel patch (`unoptimized.patch`) is for the unoptimized version of KennyLoggings. We recommend this version to understand the basic scheme, but the code is slower in practice.

# Tested Setup
We ran our evaluation using the following setup:

- Ubuntu Server 16.04 LTS
- Linux Audit installed (`sudo apt install auditd`)

# Instructions

## Kernel Module
To test KennyLoggings with the standalone kernel module, we recommend the following steps:

- Enter the `kernel-module` directory and run `make`.
- (optional) Clear the message buffer of the kernel using `sudo dmesg --clear`.
- Load `cryptomod` using `sudo insmod cryptomod.ko`.
- You can see what the kernel module did using `dmesg`.
- When you are done, unload the kernel module using `sudo rmmod cryptomod`.

## Kernel Patches
To apply the kernel patches, build and run the patched kernel, we recommend the following steps:

- Install the required packages by running `sudo apt install -y build-essential ocaml automake autoconf libtool wget python libssl-dev bc`.
- Download Ubuntu kernel 4.15.0-47 from here:
  https://launchpad.net/ubuntu/+archive/primary/+sourcefiles/linux/4.15.0-47.50/linux_4.15.0.orig.tar.gz
- Extract the downloaded kernel into a directory `linux-4.15`.
- Patch the extracted kernel using our provided kernel patch `optimized.patch` or `unoptimized.patch`. To do this, `cd` into the directory `linux-4.15` and run `patch -p1 < ../optimized.patch` or `patch -p1 < ../unoptimized.patch`.
- Compile and install the patched kernel. Instructions for this step are available in the README of the kernel itself. In short, you can run:
```sh
cp /boot/config-`uname -r` .config
make -j `nproc` && sudo make modules_install && sudo make install
```
- Reboot your machine into the custom kernel

KennyLoggings will now be on and will append proofs to all the events processed by Linux Audit.
You should see these proofs at the end of each log entry in `/var/log/audit/audit.log`.
To configure what events to log, Linux Audit provides the utility `auditctl`.
Please check out the documentation of `auditctl` to understand how to use it.
For example, to log the _read()_ system call, you can run:
```sh
sudo auditctl -a always,exit -F arch=b64 -S read,readv -b 2097152
```

# Performance Improvements
We refer the reader to the paragraph "Performance Improvements" of Section 10 of the paper for a discussion of future performance improvements for KennyLoggings.

# Troubleshooting 
The ~~log verification~~ (**edit: see update below**) and shutdown routines described in the paper are not implemented in our Linux Audit prototypes yet. Feel free to contact us if you would like to do that and we would be happy to help.

# Contact Information
Contact information is available in the [paper][paper].

# Citation
If you make any use of this code for academic purposes, please cite the paper:

```tex
@inproceedings{paccagnella2020kennyloggings,
	author = {Riccardo Paccagnella and Kevin Liao and Dave Tian and Adam Bates},
	title = {Logging to the Danger Zone: Race Condition Attacks and Defenses on System Audit Frameworks},
	booktitle = {Proc.\ of the ACM Conference on Computer and Communications Security (CCS)},
	year = {2020},
}
```

---

# Fall 2021 Update

You can find a additional artifacts at the following links (credits to Alan Andrade for working on these):

* An implementation of KennyLoggings on top of a newer version of the audit-kernel: https://github.com/Alan-S-Andrade/audit-kernel/tree/integrity_proofs
* A version of audit-userspace that supports enabling/disabling KennyLoggings at runtime: https://github.com/Alan-S-Andrade/audit-userspace/tree/kennyloggings
* A script to verify the proofs produced by KennyLoggings: https://github.com/Alan-S-Andrade/Log-Integrity-Verification


[paper]: https://rp8.web.engr.illinois.edu/papers/kennyloggings-ccs2020.pdf